import os
from multiprocessing import Pool
import subprocess
import yaml
import time
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
from concurrent.futures import ProcessPoolExecutor, TimeoutError, as_completed
import random
import string
import re
import shutil
import glob

def compute_spatial_std(model_var_file, reference_var_file):
    (model_var, model_time_series) = model_var_file
    (reference_var, reference_time_series) = reference_var_file
    if not (os.path.isfile(model_time_series) or os.path.islink(model_time_series)):
        raise Exception(f"Can not find the model data file '{model_time_series}'! Abort!")
    if not (os.path.isfile(reference_time_series) or os.path.islink(reference_time_series)):
        raise Exception(f"Can not find the reference data file '{reference_time_series}'! Abort!")
    (base_name_model, ext_model) = os.path.splitext(os.path.basename(model_time_series))
    (base_name_ref, ext_ref) = os.path.splitext(os.path.basename(reference_time_series))
    if ext_model not in ['.ncz', '.nc'] or ext_ref not in ['.ncz', '.nc']:
        raise Exception(f"Error: Unsupported file extension '{ext_model}' or '{ext_ref}' for data file '{model_time_series}' or '{reference_time_series}'! The time series should have '.nc' or '.ncz' extension! Abort!")
    ds_model = xr.open_dataset(model_time_series)
    ds_ref = xr.open_dataset(reference_time_series)
    if len(ds_model['time']) != len(ds_ref['time']):
        raise Exception(f"The model '{model_time_series}' and reference '{reference_time_series}' should have the same number of time levels to compute spatial STD! Abort!")
    var_model = np.array(ds_model[model_var])
    var_ref = np.array(ds_ref[reference_var])
    nan_mask_ref = np.isnan(var_ref)
    nan_mask_model = np.isnan(var_model)
    nan_mask = nan_mask_ref | nan_mask_model
    if np.all(nan_mask):
        raise Exception(f"The model '{model_time_series}' and reference '{reference_time_series}' datasets don't have the common gridpoints! Can not compute the spatial STD! Abort!")
    squared_differences = (var_model - var_ref) ** 2
    squared_differences[nan_mask] = 0
    sum_squared_differences = np.sum(squared_differences, axis=(1, 2))
    num_spatial_points = np.sum(~nan_mask, axis=(1, 2))
    normalized_sums = sum_squared_differences / num_spatial_points
    l2_error = np.sqrt(normalized_sums)
    return l2_error

class ReferenceDatasetCreator:

    def __init__(self, list_of_variables=None, list_of_data_files=None, list_of_resamplings=None, list_of_output_frequencies=None, list_of_scale_offset=None, work_directory='', config_file='/work/bg1155/COPAT2/evaluation/modeldata_settings_COPAT2_ex.yaml'):
        self.list_of_variables = list_of_variables if list_of_variables is not None else []
        self.list_of_data_files = list_of_data_files if list_of_data_files is not None else []
        self.list_of_resamplings = list_of_resamplings if list_of_resamplings is not None else []
        self.list_of_output_frequencies = list_of_output_frequencies if list_of_output_frequencies is not None else []
        self.list_of_scale_offset = list_of_scale_offset if list_of_scale_offset is not None else []
        self.work_directory = work_directory
        self.config_file = config_file
        if self.list_of_variables:
            self.UpdateListOfVariables(self.list_of_variables)
        if self.list_of_data_files:
            self.UpdateListOfDataFiles(self.list_of_data_files)
        if self.list_of_resamplings:
            self.UpdateListOfResamplings(self.list_of_resamplings)
        if self.list_of_output_frequencies:
            self.UpdateListOfOutputFrequencies(self.list_of_output_frequencies)
        if self.list_of_scale_offset:
            self.UpdateListOfScaleOffset(self.list_of_scale_offset)
        if self.work_directory:
            self.UpdateWorkDirectory(self.work_directory)
        os.environ['OMP_NUM_THREADS'] = '8'

    def CheckVariables(self):
        (_, file_extension) = os.path.splitext(self.config_file)
        if file_extension.lower() != '.yaml':
            raise Exception(f"The configuration file '{self.config_file}' has to have '.yaml' extension! Abort!")
        if not os.path.isfile(self.config_file):
            raise Exception(f"The configuration file '{self.config_file}' does not exist! Abort!")
        with open(self.config_file, 'r') as file:
            config = yaml.safe_load(file)
        if 'C2I200' not in config:
            raise Exception(f"Can't find 'C2I200' in config file '{self.config_file}'! Abort!")
        for var in self.list_of_variables:
            if var not in config['C2I200']['paramdict']:
                raise Exception(f"Unknown variable '{var}'")

    def UpdateListOfVariables(self, list_of_variables):
        if len(list_of_variables) == 0:
            raise Exception(f'At least 1 variable should be assigned! Abort!')
        self.list_of_variables = list_of_variables
        self.CheckVariables()

    def UpdateListOfDataFiles(self, list_of_data_files):
        if len(list_of_data_files) == 0:
            raise Exception('At least 1 data file should be given! Abort!')
        for file in list_of_data_files:
            if not (os.path.isfile(file) or os.path.islink(file)):
                raise Exception(f"Can not find the data file '{file}'! Abort!")
        for file in list_of_data_files:
            (base_name, ext) = os.path.splitext(os.path.basename(file))
            if ext not in ['.ncz', '.nc']:
                raise Exception(f"Error: Unsupported file extension '{ext}' for data file '{file}'! The time series should have '.nc' or '.ncz' extension! Abort!")
            return
        self.list_of_data_files = list_of_data_files

    def UpdateListOfResamplings(self, list_of_resamplings):
        for resampling in list_of_resamplings:
            if not (resampling == 'mean' or resampling == 'sum'):
                raise Exception(f"Resampling '{resampling}' is not available (use 'mean' or 'sum')! Abort!")
        self.list_of_resamplings = list_of_resamplings

    def UpdateListOfOutputFrequencies(self, list_of_output_frequencies):
        list_of_freqs = ['hourly', 'daily', 'monthly', 'seasonaly', 'yearly']
        for freq in list_of_output_frequencies:
            if freq not in list_of_freqs:
                raise Exception(f"Unknown frequency '{freq}'! The currently available frequencies are: {list_of_freqs}! Abort!")
        self.list_of_output_frequencies = list_of_output_frequencies

    def UpdateListOfScaleOffset(self, list_of_scale_offset):
        self.list_of_scale_offset = list_of_scale_offset

    def UpdateWorkDirectory(self, work_directory):
        if not os.path.isdir(work_directory):
            raise Exception(f"Directory '{work_directory}' does not exist and could not be used as work directory! Abort!")
        elif not os.access(work_directory, os.W_OK):
            raise Exception(f"Current user has NO rights to write to the directory '{work_directory}' so it can not be use as work directory! Abort!")
        self.work_directory = work_directory

    def LogSetup(self):
        print('############## Current setup info! ##############')
        print(f'List of variables: {self.list_of_variables}')
        print(f'List of resamplings: {self.list_of_resamplings}')
        print(f'List of output frequencies:{self.list_of_output_frequencies}')
        print(f'List of scale-offset pairs:{self.list_of_scale_offset}')
        print(f'List of data files:{self.list_of_data_files}')
        print(f"Work directory: '{self.work_directory}'")

    def CheckSetupConsistency(self):
        if len(self.list_of_variables) == 0:
            return (False, 'Zero variables selected!')
        if not all((len(arr) == len(self.list_of_variables) for arr in [self.list_of_resamplings, self.list_of_variables, self.list_of_output_frequencies, self.list_of_scale_offset])):
            return (False, "The 'list_of_variables', 'list_of_resamplings', 'list_of_output_frequencies', 'list_of_scale_offset' have different length!")
        if not bool(self.work_directory):
            return (False, "The 'work_directory' is not assigned!")
        return (True, 'The current setup is correct!')

    def PrepareWorkFolder(self):
        (is_correct, message) = self.CheckSetupConsistency()
        if not is_correct:
            raise Exception(f'Can not start preparation, setup is not correct! {message}')
        var_dirnames = [os.path.join(self.work_directory, var) for var in self.list_of_variables]
        for fld in var_dirnames:
            if not os.path.isdir(fld):
                os.makedirs(fld)
        print('#### Working directories for reference data are created! ####')
        var_info_list = [(var_dirnames[var_ind], var, self.list_of_data_files[var_ind], self.list_of_resamplings[var_ind], self.list_of_output_frequencies[var_ind], self.list_of_scale_offset[var_ind][0], self.list_of_scale_offset[var_ind][1]) for (var_ind, var) in enumerate(self.list_of_variables)]
        timer_start = time.time()
        with Pool() as pool:
            pool.map(copy_resample_rename_for_variable, var_info_list)
        timer_end = time.time()
        elapsed_time = timer_end - timer_start
        print(f'#### Resampling for reference dataset done! ({elapsed_time:.2f} s) ####')

def copy_resample_rename_for_variable(var_info):
    is_work = False
    (var_dirname, var, data_file, resampling, temporal_frequency, scale_factor, offset_value) = var_info
    current_var_resampled = os.path.join(var_dirname, var + '_ref.nc')
    if os.path.isfile(current_var_resampled):
        is_work = False
    else:
        is_work = True
        if temporal_frequency == 'hourly':
            if resampling == 'mean':
                command = ['cdo', '-O', '-b', 'F64', f'-addc,{offset_value}', f'-mulc,{scale_factor}', '-setyear,2008', '-yhourmean'] + [data_file] + [current_var_resampled]
            elif resampling == 'sum':
                command = ['cdo', '-O', '-b', 'F64', f'-addc,{offset_value}', f'-mulc,{scale_factor}', '-setyear,2008', '-hoursum', '-yhourmean'] + [data_file] + [current_var_resampled]
        elif temporal_frequency == 'daily':
            if resampling == 'mean':
                command = ['cdo', '-O', '-b', 'F64', f'-addc,{offset_value}', f'-mulc,{scale_factor}', '-setyear,2008', '-ydaymean'] + [data_file] + [current_var_resampled]
            elif resampling == 'sum':
                command = ['cdo', '-O', '-b', 'F64', f'-addc,{offset_value}', f'-mulc,{scale_factor}', '-setyear,2008', '-daysum', '-yhourmean'] + [data_file] + [current_var_resampled]
        elif temporal_frequency == 'monthly':
            if resampling == 'mean':
                command = ['cdo', '-O', '-b', 'F64', f'-addc,{offset_value}', f'-mulc,{scale_factor}', '-setyear,2008', '-ymonmean'] + [data_file] + [current_var_resampled]
            elif resampling == 'sum':
                command = ['cdo', '-O', '-b', 'F64', f'-addc,{offset_value}', f'-mulc,{scale_factor}', '-setyear,2008', '-monsum', '-yhourmean'] + [data_file] + [current_var_resampled]
        elif temporal_frequency == 'seasonaly':
            if resampling == 'mean':
                command = ['cdo', '-O', '-b', 'F64', f'-addc,{offset_value}', f'-mulc,{scale_factor}', '-setyear,2008', '-yseasmean'] + [data_file] + [current_var_resampled]
            elif resampling == 'sum':
                command = ['cdo', '-O', '-b', 'F64', f'-addc,{offset_value}', f'-mulc,{scale_factor}', '-setyear,2008', '-seassum', '-yhourmean'] + [data_file] + [current_var_resampled]
        elif temporal_frequency == 'yearly':
            if resampling == 'mean':
                command = ['cdo', '-O', '-b', 'F64', f'-addc,{offset_value}', f'-mulc,{scale_factor}', '-setyear,2008', '-timmean', '-yearmean'] + [data_file] + [current_var_resampled]
            elif resampling == 'sum':
                command = ['cdo', '-O', '-b', 'F64', f'-addc,{offset_value}', f'-mulc,{scale_factor}', '-setyear,2008', '-yearsum', '-yhourmean'] + [data_file] + [current_var_resampled]
        else:
            raise Exception("Time resampling error! Currently only 'hourly, daily, monthly, seasonaly, yearly' values are available!")
    if is_work:
        try:
            subprocess.run(command, check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        except subprocess.CalledProcessError as e:
            raise Exception(f'An error occurred while time-resampling file {current_var_resampled}! Reason: {e}. Abort!')

class LinearMetamodelCreator:

    def __init__(self, config_file='', list_of_simulations=None, list_of_periods=None, list_of_coefficients=None, list_of_variables=None, list_of_resamplings=None, list_of_output_frequencies=None, output_directory='', work_directory=''):
        self.config_file = config_file
        self.config = {}
        self.list_of_simulations = list_of_simulations if list_of_simulations is not None else []
        self.list_of_periods = list_of_periods if list_of_periods is not None else []
        self.list_of_coefficients = list_of_coefficients if list_of_coefficients is not None else []
        self.list_of_variables = list_of_variables if list_of_variables is not None else []
        self.list_of_resamplings = list_of_resamplings if list_of_resamplings is not None else []
        self.list_of_output_frequencies = list_of_output_frequencies if list_of_output_frequencies is not None else []
        self.output_directory = output_directory
        self.work_directory = work_directory
        self.max_sum_files_time_s = 60.0
        if self.config_file:
            self.UpdateConfig(self.config_file)
        if self.list_of_simulations:
            self.UpdateListOfSimulations(self.list_of_simulations)
        if self.list_of_periods:
            self.UpdateListOfPeriods(self.list_of_periods)
        if self.list_of_coefficients:
            self.UpdateListOfCoefficients(self.list_of_coefficients)
        if self.list_of_variables:
            self.UpdateListOfVariables(self.list_of_variables)
        if self.list_of_resamplings:
            self.UpdateListOfResamplings(self.list_of_resamplings)
        if self.list_of_output_frequencies:
            self.UpdateListOfOutputFrequencies(self.list_of_output_frequencies)
        if self.output_directory:
            self.UpdateOutputDirectory(self.output_directory)
        if self.work_directory:
            self.UpdateWorkDirectory(self.work_directory)
        os.environ['OMP_NUM_THREADS'] = '8'

    def UpdateConfig(self, yaml_config):
        (_, file_extension) = os.path.splitext(yaml_config)
        if file_extension.lower() != '.yaml':
            raise Exception(f"The configuration file '{yaml_config}' has to have '.yaml' extension! Abort!")
        if not os.path.isfile(yaml_config):
            raise Exception(f"The configuration file '{yaml_config}' does not exist! Abort!")
        self.config_file = yaml_config
        with open(yaml_config, 'r') as file:
            self.config = yaml.safe_load(file)

    def UpdateListOfSimulations(self, list_of_simulations):
        if len(list_of_simulations) == 0:
            raise Exception(f'At least 1 simulation should be assigned! Abort!')
        for sim in list_of_simulations:
            if sim not in self.config:
                raise Exception(f"'{sim}' is not presented in configuration file! Abort!")
        self.list_of_simulations = list_of_simulations

    def UpdateListOfCoefficients(self, list_of_coefficients):
        for coef in list_of_coefficients:
            if not isinstance(coef, (int, float)):
                raise Exception('Not all coefficients are numbers! Abort!')
        self.list_of_coefficients = list_of_coefficients

    def UpdateListOfVariables(self, list_of_variables):
        if len(list_of_variables) == 0:
            raise Exception(f'At least 1 variable should be assigned! Abort!')
        if not bool(self.config_file):
            raise Exception(f'The config file should be assigned at first! Abort!')
        if 'C2I250c' not in self.config:
            raise Exception(f"Can't find 'C2I250c' in config file '{self.config_file}'! Abort!")
        for var in list_of_variables:
            if var not in self.config['C2I250c']['paramdict']:
                raise Exception(f"Variable '{var}' is not in the paramdict of simulations! Abort!")
        self.list_of_variables = list_of_variables

    def UpdateListOfPeriods(self, list_of_periods):
        for period in list_of_periods:
            if not re.match('^\\d{4}-\\d{4}$', period):
                raise Exception(f"Period '{period}' does not match the pattern 'YYYY-YYYY'! Abort!")
        self.list_of_periods = list_of_periods

    def UpdateListOfResamplings(self, list_of_resamplings):
        for resampling in list_of_resamplings:
            if not (resampling == 'mean' or resampling == 'sum'):
                raise Exception(f"Resampling '{resampling}' is not available (use 'mean' or 'sum')! Abort!")
        self.list_of_resamplings = list_of_resamplings

    def UpdateOutputDirectory(self, output_directory):
        if not os.path.isdir(output_directory):
            raise Exception(f"Directory '{output_directory}' does not exist and could not be used as output directory! Abort!")
        elif not os.access(output_directory, os.W_OK):
            raise Exception(f"Current user has NO rights to write to the directory '{output_directory}' so it can not be use as output directory! Abort!")
        self.output_directory = output_directory

    def UpdateWorkDirectory(self, work_directory):
        if not os.path.isdir(work_directory):
            raise Exception(f"Directory '{work_directory}' does not exist and could not be used as work directory! Abort!")
        elif not os.access(work_directory, os.W_OK):
            raise Exception(f"Current user has NO rights to write to the directory '{work_directory}' so it can not be use as work directory! Abort!")
        self.work_directory = work_directory

    def UpdateListOfOutputFrequencies(self, list_of_output_frequencies):
        list_of_freqs = ['hourly', 'daily', 'monthly', 'seasonaly', 'yearly']
        for freq in list_of_output_frequencies:
            if freq not in list_of_freqs:
                raise Exception(f"Unknown frequency '{freq}'! The currently available frequencies are: {list_of_freqs}! Abort!")
        self.list_of_output_frequencies = list_of_output_frequencies

    def LogSetup(self):
        print('############## Current setup info! ##############')
        print(f"Configuration file: '{self.config_file}'")
        print(f'List of simulations: {self.list_of_simulations}')
        print(f'List of periods: {self.list_of_periods}')
        print(f'List of coefficients: {self.list_of_coefficients}')
        print(f'List of variables: {self.list_of_variables}')
        print(f'List of resamplings: {self.list_of_resamplings}')
        print(f"List of output frequencies:'{self.list_of_output_frequencies}'")
        print(f"Output directory: '{self.output_directory}'")
        print(f"Work directory: '{self.work_directory}'")

    def CheckSetupConsistency(self):
        if len(self.list_of_simulations) == 0 or len(self.list_of_variables) == 0:
            return (False, 'Zero simulations or variables selected!')
        if not all((len(arr) == len(self.list_of_simulations) for arr in [self.list_of_simulations, self.list_of_periods])):
            return (False, "The 'list_of_simulations', 'list_of_coefficients', 'list_of_periods' have different length!")
        if not all((len(arr) == len(self.list_of_variables) for arr in [self.list_of_resamplings, self.list_of_variables, self.list_of_output_frequencies])):
            return (False, "The 'list_of_variables', 'list_of_resamplings', 'list_of_output_frequencies' have different length!")
        if not bool(self.output_directory):
            return (False, "The 'output_directory' is not assigned!")
        if not bool(self.work_directory):
            return (False, "The 'work_directory' is not assigned!")
        return (True, 'The current setup is correct!')

    def CheckCoeffsConsistency(self):
        (is_setup, err_message) = self.CheckSetupConsistency()
        if not is_setup:
            return (False, err_message)
        if not all((len(arr) == len(self.list_of_simulations) for arr in [self.list_of_simulations, self.list_of_coefficients, self.list_of_periods])):
            return (False, "The 'list_of_simulations', 'list_of_coefficients', 'list_of_periods' have different length!")
        return (True, 'The list of coefficints has the right length!')

    def PrepareWorkFolder(self):
        (is_correct, message) = self.CheckSetupConsistency()
        if not is_correct:
            raise Exception(f'Can not start preparation, setup is not correct! {message}')
        sim_dirnames = [os.path.join(self.work_directory, sim_id) for sim_id in self.list_of_simulations]
        timer_start = time.time()
        with Pool() as pool:
            pool.map(create_directory, sim_dirnames)
            for current_dir in sim_dirnames:
                pool.map(create_directory, [os.path.join(current_dir, var) for var in self.list_of_variables])
        timer_end = time.time()
        elapsed_time = timer_end - timer_start
        print(f'#### Working directories for simulations are created ({elapsed_time:.2f} s) ####')
        timer_start = time.time()
        var_info_list = [(sim, sim_dirnames[sim_ind], self.config, var, self.list_of_periods[sim_ind]) for var in self.list_of_variables for (sim_ind, sim) in enumerate(self.list_of_simulations)]
        with Pool() as pool:
            pool.map(copy_files_for_simulation_for_variable, var_info_list)
        timer_end = time.time()
        elapsed_time = timer_end - timer_start
        print(f'#### Copying simulations to work folder done! ({elapsed_time:.2f} s) ####')
        for (sim_ind, sim) in enumerate(self.list_of_simulations):
            timer_start = time.time()
            var_info_list = [(sim_dirnames[sim_ind], var, self.list_of_periods[sim_ind], self.list_of_resamplings[var_ind], self.list_of_output_frequencies[var_ind]) for (var_ind, var) in enumerate(self.list_of_variables)]
            with Pool() as pool:
                pool.map(merge_slice_aggregate_for_simulation_for_variable, var_info_list)
            timer_end = time.time()
            elapsed_time = timer_end - timer_start
            print(f'#### Resampling for simulation {sim_ind + 1}/{len(self.list_of_simulations)} done! ({elapsed_time:.2f} s) ####')

    def PrepareOutputFolder(self):
        (is_correct, message) = self.CheckSetupConsistency()
        if not is_correct:
            raise Exception(f'Can not start preparation of output folder, setup is not correct! {message}')
        output_folders = [os.path.join(self.output_directory, var) for var in self.list_of_variables]
        for folder in output_folders:
            if os.path.isdir(folder):
                for filename in os.listdir(folder):
                    file_path = os.path.join(folder, filename)
                    try:
                        if os.path.isfile(file_path) or os.path.islink(file_path):
                            os.unlink(file_path)
                        elif os.path.isdir(file_path):
                            shutil.rmtree(file_path)
                    except Exception as e:
                        raise Exception(f'Failed to delete {file_path}. Reason: {e}. Abort!')
            else:
                os.makedirs(folder)

    def ComputeLinearCombination(self):
        (is_correct, message) = self.CheckCoeffsConsistency()
        if not is_correct:
            raise Exception(f'Can not compute linear combination! {message}')
        sim_dirnames = [os.path.join(self.work_directory, sim_id) for sim_id in self.list_of_simulations]
        output_folders = [os.path.join(self.output_directory, var) for var in self.list_of_variables]
        timer_start = time.time()
        var_info_list = [(sim, output_folders[var_ind], sim_dirnames[sim_ind], var, self.list_of_coefficients[sim_ind]) for (sim_ind, sim) in enumerate(self.list_of_simulations) for (var_ind, var) in enumerate(self.list_of_variables)]
        with Pool() as pool:
            pool.map(multiply_for_simulation_for_variable, var_info_list)
        timer_end = time.time()
        elapsed_time = timer_end - timer_start
        print(f'#### The time series are multiplied by the coefficients ({elapsed_time:.2f} s) ####')
        timer_start = time.time()
        with ProcessPoolExecutor() as executor:
            futures = {executor.submit(sum_folder, folder): folder for folder in output_folders}
        for future in futures:
            try:
                result = future.result(timeout=self.max_sum_files_time_s)
            except TimeoutError:
                print(f'Process for folder {futures[future]} timed out after {timeout_seconds} seconds')
            except Exception as e:
                print(f'An error occurred for folder {futures[future]}: {e}')
        timer_end = time.time()
        elapsed_time = timer_end - timer_start
        print(f'#### The time series are added up ({elapsed_time:.2f} s) ####')

def check_sim_var_file_ready(var, workdir):
    if os.path.isfile(os.path.join(workdir, var, var + '.nc')):
        return True
    else:
        return False

def multiply_for_simulation_for_variable(var_info):
    (sim, output_folder_for_var, sim_folder, var, coefficient) = var_info
    input_file = os.path.join(sim_folder, var, var + '.nc')
    output_file = os.path.join(output_folder_for_var, sim + '.nc')
    command = ['cdo', '-O', '-b', 'F64', f'-mulc,{coefficient}', input_file, output_file]
    try:
        subprocess.run(command, check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError as e:
        raise Exception(f"An error occurred while multiplying file '{input_file}' by coefficient! Reason: {e}. Abort!")

def generate_random_string(length):
    characters = string.ascii_letters + string.digits
    random_string = ''.join((random.choice(characters) for _ in range(length)))
    return random_string

def add_files(file_pair, iteration):
    filename = f'{generate_random_string(10)}.nc'
    output_file = os.path.join(os.path.dirname(file_pair[0]), filename)
    command = ['cdo', 'add', file_pair[0], file_pair[1], output_file]
    try:
        subprocess.run(command, check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError as e:
        raise Exception(f"An error occurred while computing the sum of '{file_pair[0]}' and '{file_pair[1]}'! Reason: {e}. Abort!")
    return output_file

def parallel_sum(files_input):
    files = files_input.copy()
    iteration = 0
    temp_files = files.copy()
    while len(files) > 1:
        iteration += 1
        extra_file = files.pop() if len(files) % 2 != 0 else None
        file_pairs = [(files[i], files[i + 1]) for i in range(0, len(files), 2)]
        with Pool() as pool:
            new_files = pool.starmap(add_files, [(pair, iteration) for pair in file_pairs])
        temp_files += new_files
        files = new_files
        if extra_file:
            files.append(extra_file)
    final_file = files[0]
    for temp_file in temp_files:
        if temp_file != final_file:
            try:
                os.remove(temp_file)
            except Exception as e:
                raise Exception(f"Failed to delete temporary file '{temp_file}'. Reason: {e}. Abort!")
    return final_file

def get_netcdf_files(folder):
    return [os.path.join(folder, f) for f in os.listdir(folder) if f.endswith('.nc')]

def sum_folder(folder):
    files = get_netcdf_files(folder)
    if not files:
        raise ValueError(f'No NetCDF files found in folder: {folder}')
    result_file = parallel_sum(files)
    new_filename = os.path.join(os.path.dirname(result_file), os.path.dirname(result_file).split('/')[-1] + '_lc.nc')
    try:
        subprocess.run(['mv', result_file, new_filename], check=True)
    except Exeption as e:
        raise Exception(f"Failed to rename file '{result_file}'. Reason: {e}. Abort!")

def copy_files_for_simulation_for_variable(var_info):
    (sim, sim_dirname, config, var, periods) = var_info
    if not check_sim_var_file_ready(var, sim_dirname):
        current_sim_storage = config[sim]['storagedir']
        (current_sim_start_year, current_sim_end_year) = map(int, periods.split('-'))
        current_var_pattern = config[sim]['storagefil'][config[sim]['paramdict'][var]]
        current_var_file_list = glob.glob(os.path.join(current_sim_storage, current_var_pattern))
        if not any(os.scandir(os.path.join(sim_dirname, var))):
            for file in current_var_file_list:
                if check_years(file, current_sim_start_year, current_sim_end_year):
                    copy_file(file, os.path.join(sim_dirname, var))

def merge_slice_aggregate_for_simulation_for_variable(var_info):
    is_work = False
    (sim_dirname, var, periods, resampling, temporal_frequency) = var_info
    (year_start, year_end) = map(int, periods.split('-'))
    current_var_dir = os.path.join(sim_dirname, var)
    netcdf_files = [os.path.join(current_var_dir, f) for f in os.listdir(current_var_dir) if f.endswith('.nc') or f.endswith('.ncz')]
    if not netcdf_files:
        raise Exception(f"No NetCDF files found in the directory: '{current_var_dir}'! Abort!")
    if temporal_frequency == 'hourly':
        current_var_resampled = os.path.join(current_var_dir, var + '.nc')
        if not os.path.isfile(current_var_resampled):
            is_work = True
            if resampling == 'mean':
                command = ['cdo', '-O', '-b', 'F64', '-setyear,2008', '-yhourmean', f'-seldate,{year_start}-01-01,{year_end}-12-31', '-mergetime'] + netcdf_files + [current_var_resampled]
            elif resampling == 'sum':
                command = ['cdo', '-O', '-b', 'F64', '-setyear,2008', '-hoursum', '-yhourmean', f'-seldate,{year_start}-01-01,{year_end}-12-31', '-mergetime'] + netcdf_files + [current_var_resampled]
    elif temporal_frequency == 'daily':
        current_var_resampled = os.path.join(current_var_dir, var + '.nc')
        if not os.path.isfile(current_var_resampled):
            is_work = True
            if resampling == 'mean':
                command = ['cdo', '-O', '-b', 'F64', '-setyear,2008', '-ydaymean', f'-seldate,{year_start}-01-01,{year_end}-12-31', '-mergetime'] + netcdf_files + [current_var_resampled]
            elif resampling == 'sum':
                command = ['cdo', '-O', '-b', 'F64', '-setyear,2008', '-daysum', '-yhourmean', f'-seldate,{year_start}-01-01,{year_end}-12-31', '-mergetime'] + netcdf_files + [current_var_resampled]
    elif temporal_frequency == 'monthly':
        current_var_resampled = os.path.join(current_var_dir, var + '.nc')
        if not os.path.isfile(current_var_resampled):
            is_work = True
            if resampling == 'mean':
                command = ['cdo', '-O', '-b', 'F64', '-setyear,2008', '-ymonmean', f'-seldate,{year_start}-01-01,{year_end}-12-31', '-mergetime'] + netcdf_files + [current_var_resampled]
            elif resampling == 'sum':
                command = ['cdo', '-O', '-b', 'F64', '-setyear,2008', '-monsum', '-yhourmean', f'-seldate,{year_start}-01-01,{year_end}-12-31', '-mergetime'] + netcdf_files + [current_var_resampled]
    elif temporal_frequency == 'seasonaly':
        current_var_resampled = os.path.join(current_var_dir, var + '.nc')
        if not os.path.isfile(current_var_resampled):
            is_work = True
            if resampling == 'mean':
                command = ['cdo', '-O', '-b', 'F64', '-setyear,2008', '-yseasmean', f'-seldate,{year_start}-01-01,{year_end}-12-31', '-mergetime'] + netcdf_files + [current_var_resampled]
            elif resampling == 'sum':
                command = ['cdo', '-O', '-b', 'F64', '-setyear,2008', '-seassum', '-yhourmean', f'-seldate,{year_start}-01-01,{year_end}-12-31', '-mergetime'] + netcdf_files + [current_var_resampled]
    elif temporal_frequency == 'yearly':
        current_var_resampled = os.path.join(current_var_dir, var + '.nc')
        if not os.path.isfile(current_var_resampled):
            is_work = True
            if resampling == 'mean':
                command = ['cdo', '-O', '-b', 'F64', '-setyear,2008', '-timmean', '-yearmean', f'-seldate,{year_start}-01-01,{year_end}-12-31', '-mergetime'] + netcdf_files + [current_var_resampled]
            elif resampling == 'sum':
                command = ['cdo', '-O', '-b', 'F64', '-setyear,2008', '-yearsum', '-yhourmean', f'-seldate,{year_start}-01-01,{year_end}-12-31', '-mergetime'] + netcdf_files + [current_var_resampled]
    else:
        raise Exception("Time resampling error! Currently only 'hourly, daily, monthly, seasonaly, yearly' values are available!")
    if is_work:
        try:
            subprocess.run(command, check=True)
        except subprocess.CalledProcessError as e:
            raise Exception(f'An error occurred while time-resampling file {current_var_resampled}! Reason: {e}. Abort!')
        for file in netcdf_files:
            os.remove(file)

def create_directory(dir_path):
    if not os.path.isdir(dir_path):
        os.makedirs(dir_path)

def check_years(file_path, start_year, end_year):
    try:
        result = subprocess.run(['cdo', 'showyear', file_path], check=True, capture_output=True, text=True)
        first_line = result.stdout.splitlines()[0].strip()
        years = [int(year.strip()) for year in first_line.split()]
    except subprocess.CalledProcessError as e:
        raise Exception(f'Error occurred (check_year): {e.stderr}! Abort!')
        return None
    except ValueError as e:
        print(f'Error converting output to integer (check_year): {e}! Abort!')
        return None
    if min(years) > end_year or max(years) < start_year:
        return False
    else:
        return True

def copy_file(file_path, dest_dir_path):
    if not (os.path.isfile(file_path) or os.path.islink(file_path)):
        raise Exception(f"Filename '{file_path}' does not exist! Abort!")
    if not os.path.isdir(dest_dir_path):
        raise Exception(f"Directory '{dest_dir_path}' does not exist! Abort!")
    (base_name, ext) = os.path.splitext(os.path.basename(file_path))
    if ext not in ['.ncz', '.nc']:
        raise Exception(f"Error: Unsupported file extension {ext}! The time series should have '.nc' or '.ncz' extension. Abort!")
        return
    dest_file_path = os.path.join(dest_dir_path, base_name + '.nc')
    command = ['nccopy', '-d', '0', file_path, dest_file_path]
    try:
        subprocess.run(command, check=True)
    except subprocess.CalledProcessError as e:
        raise Exception(f"An error occurred while copying the file '{file_path}'! Reason: {e}. Abort!")

