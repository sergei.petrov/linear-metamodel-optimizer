import os
import json
import ast
from collections import OrderedDict
import numpy as np



def is_function_or_class(node):
    return isinstance(node, (ast.FunctionDef, ast.ClassDef))

def test_func():
    print np.pi

def extract_imports_and_definitions(code):
    tree = ast.parse(code)
    imports = []
    definitions = []
    
    for node in tree.body:
        if is_import_statement(node):
            imports.append(ast.unparse(node).strip())
        elif is_function_or_class(node):
            definitions.append(ast.unparse(node).strip())
    
    return imports, definitions

def parse_notebook(notebook_path):
    with open(notebook_path, 'r', encoding='utf-8') as file:
        notebook = json.load(file)
    
    imports = []
    definitions = []
    
    for cell in notebook['cells']:
        if cell['cell_type'] == 'code':
            code = ''.join(cell['source'])
            cell_imports, cell_definitions = extract_imports_and_definitions(code)
            imports.extend(cell_imports)
            definitions.extend(cell_definitions)
    
    return imports, definitions

def create_library():
    # Get the directory where the script is located
    script_dir = os.path.dirname(os.path.abspath(__file__))
    
    # Move one level up to the base directory
    base_dir = os.path.dirname(script_dir)
    
    # Define paths relative to the base directory
    notebooks_path = os.path.join(base_dir, 'Notebooks')
    library_path = os.path.join(base_dir, 'Library', 'LMopt.py')
    
    all_imports = OrderedDict()
    all_definitions = []

    for notebook_filename in os.listdir(notebooks_path):
        if notebook_filename.endswith('.ipynb'):
            notebook_path = os.path.join(notebooks_path, notebook_filename)
            imports, definitions = parse_notebook(notebook_path)
            for imp in imports:
                all_imports[imp] = None  # Using OrderedDict to remove duplicates and keep order
            all_definitions.extend(definitions)
    
    with open(library_path, 'w', encoding='utf-8') as lib_file:
        # Write imports
        for imp in all_imports.keys():
            lib_file.write(imp + '\n')
        lib_file.write('\n')
        
        # Write definitions
        for definition in all_definitions:
            lib_file.write(definition + '\n\n')

if __name__ == '__main__':
    create_library()
